QT += core testlib
QT -= gui

TARGET = Forel
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

TEMPLATE = app

SOURCES += main.cpp

LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui -lamv
