#include <QCoreApplication>
#include <opencv2/opencv.hpp>
#include <QSet>
#include <vector>
#include <iostream>
#include <QCryptographicHash>
#include <QTest>

using namespace cv;
using namespace std;

struct structPixel
{
    uint8_t brightness;
    uint8_t x;
    uint8_t y;

inline bool operator ==(const structPixel & pixel) const
{
   return (brightness==pixel.brightness && x==pixel.x && y==pixel.y);
}
};

double distance(Vec3b parFirstPixel,Vec3b parSecondPixel)
{
    //для цветного
    /*if (distance(frame.at<Vec3b>(rowIndex,colIndex),frame.at<Vec3b>(i,j))<R)
    {
        _labels.at<uint8_t>(i,j) = 255;
    }*/
    return sqrt((parFirstPixel[0] - parSecondPixel[0])*(parFirstPixel[0] - parSecondPixel[0])
                + (parFirstPixel[1] - parSecondPixel[1])*(parFirstPixel[1] - parSecondPixel[1])
                + (parFirstPixel[2] - parSecondPixel[2])*(parFirstPixel[2] - parSecondPixel[2]));
}
QSet<int> initSet(Mat parFrame)
 {
     QSet<int > pixelsSet;
     for (int i=0; i<parFrame.rows; i++)
         for (int j=0; j<parFrame.cols; j++)
         {
            pixelsSet.insert(parFrame.at<uint8_t>(i,j));
         }
     return pixelsSet;
 }
/*QSet<structPixel> initSet(Mat parFrame)
 {
     QSet<structPixel > pixelsSet;
     for (int i=0; i<parFrame.cols; i++)
         for (int j=0; j<parFrame.rows; j++)
         {
            structPixel pixel;
            pixel.brightness=parFrame.at<uint8_t>(i,j);
            pixel.x=i;
            pixel.y=j;
            pixelsSet.insert(pixel);
         }
     return pixelsSet;
 }*/
/*uint qHash(const structPixel &it)
 {
     QCryptographicHash ch(QCryptographicHash::Md4);
     ch.addData((const char *)&it.brightness,sizeof(it.brightness));
     ch.addData((const char *)&it.x,sizeof(it.x));
     ch.addData((const char *)&it.y,sizeof(it.y));
     return ch.result().toUInt();
 }*/
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString filename="/home/alexandra/trash/exp1.JPG";
    VideoCapture capture;

    if(! capture.open(filename.toStdString()))
        throw 1;
    Mat frame;
    int R=51;//радиус поиска локальных сгущений(примерно на пять классов)
    capture.read(frame);
    double center;
    double newCenter=0;//для серого
    int pixelInCluster=0;
    int k=0;
    bool isFirst=true;
    //QSet<structPixel > pixelsSet;
    //QSet<int> pixelsSet;
    qsrand(time(NULL));
    //QSet<int > cluster;
    int u=frame.rows*frame.cols;
    double eps=0.001;
    do
    {
        //vector <Mat> labels;//Принадлежность пикселей к кластерам
        Mat result=Mat::zeros(frame.rows,frame.cols,CV_8UC1);
        Mat labels; //промежуточные метки

        Mat binary;
        cvtColor(frame, binary, CV_BGR2GRAY);

        //pixelsSet=initSet(binary);
        //while(pixelsSet.count()!= 0)//пока не будет кластеризована вся выборка
        while(u!=0)
        {
            do
            {
                labels=Mat::zeros(frame.rows,frame.cols,CV_8UC1);
                pixelInCluster=0;

                //cluster.clear();
                if(isFirst)
                {
                    int colIndex;
                    int rowIndex;
                    do
                    {

                    colIndex = qrand() / (double) INT_MAX * frame.cols;
                    rowIndex = qrand() / (double) INT_MAX * frame.rows;

                    center = binary.at<uint8_t>(rowIndex,colIndex);

                    //center =qrand() % 255;
                   }
                    while (result.at<uint8_t>(rowIndex,colIndex != 0));

                    isFirst=false;
                }
                else
                {
                   center= newCenter;
                   newCenter=0;
                   pixelInCluster=0;
                }
                 //пометить объекты выборки на расстоянии меньшим R (п.2)
                for (int i=0; i<binary.rows; i++)
                    for (int j=0; j<binary.cols; j++)
                    {
                        if (result.at<uint8_t>(i,j) == 0)
                        {
                            uint8_t pixel=binary.at<uint8_t>(i,j);
                            if (abs(center-pixel)<R)
                            {
                                labels.at<uint8_t>(i,j) = 20*(k+1);
                                pixelInCluster++;
                                newCenter+=pixel;
                                //cluster.insert(pixel);
                            }
                        }
                    }
                //вычислить центр тяжести (п.3)
                if (pixelInCluster != 0)
                {
                    newCenter /= (double)pixelInCluster;
                }
            }
            while ((newCenter-center)>eps);
            isFirst=true;
            k++;
            u-=pixelInCluster;
           // pixelsSet.subtract(cluster);
            for (int i=0; i<binary.rows; i++)
                for (int j=0; j<binary.cols; j++)
                {
                   if (labels.at<uint8_t>(i,j) != 0)
                   {
                       result.at<uint8_t>(i,j)=labels.at<uint8_t>(i,j);
                   }
                }
        }

        imshow("result",result);
        imshow("binary",binary);
        QTest::qWait(40);
        break;
    }
    while (capture.read(frame));
    return a.exec();
}

